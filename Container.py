print("list")

# 列表
my_list = [1, 2, 3, 4, 5]
print("Original List:", my_list)

# 新增元素到列表
my_list.append(6)
print("List after append(6):", my_list)

my_list.insert(0, 0)
print("List after insert(0, 0):", my_list)

# 刪除元素
my_list.remove(3)
print("List after remove(3):", my_list)

popped_value = my_list.pop(2)
print("Popped value:", popped_value)
print("List after pop(2):", my_list)

del my_list[1]
print("List after del my_list[1]:", my_list)

# 存取元素
element = my_list[3]
print("Element at index 3:", element)

sublist = my_list[1:4]
print("Sublist from index 1 to 3:", sublist)

# 列表長度
length = len(my_list)
print("Length of the list:", length)

# 判斷元素是否存在於列表中
is_present = 5 in my_list
print("Is 5 present in the list?", is_present)

# 清空列表
my_list.clear()
print("List after clear():", my_list)

print("")
print("tuple")

# 元組
my_tuple = (10, 20, 30)
print("\nOriginal Tuple:", my_tuple)

# 存取元素
element = my_tuple[1]
print("Element at index 1:", element)

# 元組長度
length = len(my_tuple)
print("Length of the tuple:", length)

# 判斷元素是否存在於元組中
is_present = 20 in my_tuple
print("Is 20 present in the tuple?", is_present)

print("")
print("set")

# 集合
my_set = {1, 2, 3, 4, 5}
print("\nOriginal Set:", my_set)

# 新增元素到集合
my_set.add(6)
print("Set after add(6):", my_set)

# 刪除元素
my_set.remove(3)
print("Set after remove(3):", my_set)

my_set.discard(4)
print("Set after discard(4):", my_set)

popped_value = my_set.pop()
print("Popped value:", popped_value)
print("Set after pop():", my_set)

# 判斷元素是否存在於集合中
is_present = 5 in my_set
print("Is 5 present in the set?", is_present)

# 清空集合
my_set.clear()
print("Set after clear():", my_set)

print("")
print("dict")

# 字典
my_dict = {'name': 'Alice', 'age': 30}
print("\nOriginal Dictionary:", my_dict)

# 存取值
name = my_dict['name']
print("Value for 'name':", name)

# 新增或更新鍵值對
my_dict['city'] = 'New York'
print("Dictionary after adding 'city':", my_dict)

# 刪除鍵值對
del my_dict['age']
print("Dictionary after deleting 'age':", my_dict)

# 取得所有鍵和值
keys = my_dict.keys()
values = my_dict.values()
print("Keys:", list(keys))
print("Values:", list(values))

# 判斷鍵是否存在於字典中
is_present = 'name' in my_dict
print("Is 'name' present in the dictionary?", is_present)

# 清空字典
my_dict.clear()
print("Dictionary after clear():", my_dict)
