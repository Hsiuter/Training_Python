# 整數
my_integer = 42
negative_integer = -10

# 浮點數
my_float = 3.14
another_float = -0.5

# 字串
my_string = "Hello, World!"
another_string = 'Python'

# 布林值
is_true = True
is_false = False

# 列表
my_list = [1, "two", 3.0, [4, 5]]

# 元組
my_tuple = (10, "eleven", 12.0)

# 集合
my_set = {1, 2, 3, 4, 5}

# 字典
my_dict = {'name': 'Alice', 'age': 30}

# 複數
my_complex = 2 + 3j
another_complex = -1 - 4j

# 位元組
my_bytes = b'hello'

# 字節串
my_bytearray = bytearray([65, 66, 67])

# 空值
my_none = None

# 印出各種資料型態的值
print(my_integer)
print(my_float)
print(my_string)
print(is_true)
print(my_list)
print(my_tuple)
print(my_set)
print(my_dict)
print(my_complex)
print(my_bytes)
print(my_bytearray)
print(my_none)
