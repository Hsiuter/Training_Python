# 使用者輸入一個整數
user_input = int(input("請輸入一個整數: "))

# 檢查輸入是否為正數、負數或零
if user_input > 0:
    print("您輸入的是正數。")
elif user_input < 0:
    print("您輸入的是負數。")
else:
    print("您輸入的是零。")

# 使用for迴圈列印出1到輸入的整數的平方
if user_input > 0:
    print("前", user_input, "個正整數的平方:")
    for i in range(1, user_input + 1):
        print(i, "的平方是", i ** 2)
elif user_input < 0:
    print("請輸入正數以計算平方。")

# 使用while迴圈找出最大的2的冪次方，使其不超過輸入的整數
if user_input > 0:
    power_of_two = 1
    while power_of_two * 2 <= user_input:
        power_of_two *= 2
    print("最大的2的冪次方，不超過您輸入的整數，是", power_of_two)
elif user_input < 0:
    print("請輸入正數以計算最大的2的冪次方。")
else:
    print("0沒有最大的2的冪次方。")
