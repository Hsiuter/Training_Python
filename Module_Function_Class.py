# 模組示例：math模組
import math

# 函式示例：計算圓的面積
def calculate_circle_area(radius):
    return math.pi * radius ** 2

# 類別示例：建立圓形物件
class Circle:
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return math.pi * self.radius ** 2

# 使用模組中的數學函式
angle = 45
radians = math.radians(angle)
print(f"{angle}度等於 {radians} 弧度")

# 使用自定義函式計算圓的面積
radius = 5
area = calculate_circle_area(radius)
print(f"半徑為{radius}的圓的面積是{area}")

# 使用自定義類別建立圓形物件並計算面積
circle1 = Circle(7)
circle2 = Circle(10)
print(f"圓1的半徑: {circle1.radius}, 面積: {circle1.area()}")
print(f"圓2的半徑: {circle2.radius}, 面積: {circle2.area()}")
