import re

text = "Hello, my email is example@example.com"

# 使用正規表示式來匹配email
pattern = r'\w+@\w+\.\w+'
matches = re.findall(pattern, text)

for match in matches:
    print("Found email:", match)
